package com.choucair.formacion.definition;

import java.io.FileReader;
import java.util.List;
import java.util.Properties;
import com.choucair.formacion.steps.IseriesMyExtraSteps;
import cucumber.api.DataTable;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class IseriesMyExtraDefinition {
	private String user;
	private String password;
	@Steps
	IseriesMyExtraSteps iseriesMyExtraSteps;

	@Given("^Abrir MyExtra \"([^\"]*)\"$")
	public void abrir_MyExtra(String rutaCalidad) throws Throwable {
		iseriesMyExtraSteps.Abrir_Extra(rutaCalidad);
	}

	@When("^Autenticar MyExtra$")
	public void autenticar_MyExtra() throws Throwable {
		Properties prop = new Properties();
		prop.load(new FileReader("../com.choucair.base/dbconfig.properties"));
		this.user = prop.getProperty("db.user");
		this.password = prop.getProperty("db.password");

		iseriesMyExtraSteps.Autenticar_Extra(user, password);
	}
	
	@When("^Ejecuto los comandos para la consulta del prestamo$")
	public void ejecuto_los_comandos_para_la_consulta_del_prestamo(DataTable DataAS) throws Throwable {
		
		List<List<String>> data = DataAS.raw();   
		iseriesMyExtraSteps.ejecuto_los_comandos_para_la_consulta_del_prestamo(data);
	}

	@Then("^Verifico consulta exitosa$")
	public void verifico_consulta_exitosa() throws Throwable {
		iseriesMyExtraSteps.Verificar_Consulta();
	}
}
