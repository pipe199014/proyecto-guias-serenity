package com.choucair.formacion.steps;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.choucair.formacion.pageobjects.BackendAs400db2Page;

import net.thucydides.core.annotations.Step;

public class BackendAs400db2Steps 
{
	BackendAs400db2Page backendAs400db2Page1;

	@Step
	public void Consultar_CNAME(List<List<String>> data) throws SQLException 
	{
		BackendAs400db2Page  bAS400Page = new BackendAs400db2Page();
		
		//*Crear query
		String strDocumento = data.get(0).get(0);
		String npr = data.get(0).get(1);
		String query = bAS400Page.Armar_Query_Consulta_CNAME(strDocumento, npr);
		// Ejecuta consulta sql
		ResultSet rs = bAS400Page.Ejecutar_Query(query);
		// Verificar resultados
		bAS400Page.Verificar_Consulta_CNAME(rs, data);
	}
}
