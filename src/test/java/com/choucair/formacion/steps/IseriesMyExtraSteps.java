package com.choucair.formacion.steps;

import java.util.List;
import com.choucair.formacion.pageobjects.IseriesMyExtraPage;
import net.thucydides.core.annotations.Step;

public class IseriesMyExtraSteps {
	IseriesMyExtraPage iseriesMyExtraPage;

	@Step
	public void Abrir_Extra(String rutaCalidad) throws InterruptedException {
		iseriesMyExtraPage.iniciar_Extra(rutaCalidad);
	}

	@Step
	public void Autenticar_Extra(String strUsuario, String strClave) throws InterruptedException {
		if (iseriesMyExtraPage.Sess0 != null) {
			iseriesMyExtraPage.Autenticar_Extra(strUsuario, strClave);
		} else {
			System.out.println("No es posible ejecutar no se cuenta con una sesion activa");
		}
	}
	
	public void ejecuto_los_comandos_para_la_consulta_del_prestamo(List<List<String>> data) 
	{
		String strOpciopn = data.get(1).get(0);
		String strNum_Prestamo = data.get(1).get(1);
		iseriesMyExtraPage.Ejecutar_comandos(strOpciopn, strNum_Prestamo);
	}
	
	public void Verificar_Consulta()  throws InterruptedException
	{
		iseriesMyExtraPage.verificar_consulta_exitosa();
	}
}
