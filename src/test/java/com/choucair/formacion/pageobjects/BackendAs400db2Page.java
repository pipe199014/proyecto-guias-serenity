package com.choucair.formacion.pageobjects;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import java.util.List;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.choucair.formacion.utilities.Sql_Execute;

public class BackendAs400db2Page 
{
	public String Armar_Query_Consulta_CNAME(String strDocumento, String npr) {
		String strQuery = "SELECT * FROM SUFSDAAS.SIIL01 WHERE L01NPR = '<npr>'";
		strQuery = strQuery.replace("<npr>", npr);
		return strQuery;
	}
	
	public ResultSet Ejecutar_Query(String Query) throws SQLException
	{
		Sql_Execute DAO = new Sql_Execute();
		ResultSet rs = DAO.sql_Execute(Query);
		return rs;
	}
	
	public void Verificar_Consulta_CNAME(ResultSet rs, List<List<String>> data) throws SQLException
	{
		while (rs.next())
		{
			String Npr_Recibido = rs.getString(1);
			String Npr_Esperado = data.get(0).get(1);
			assertThat(Npr_Recibido, equalTo(Npr_Esperado));			
			String Npo_Recibido = rs.getString(3);
			String Npo_Esperado = data.get(0).get(2);
			assertThat(Npo_Recibido.trim(), equalTo(Npo_Esperado.trim()));
			String Age_Recibido = rs.getString(5);
			String Age_Esperado = data.get(0).get(3);
			assertThat(Age_Recibido.trim(), equalTo(Age_Esperado.trim()));

 		}
	}
}
