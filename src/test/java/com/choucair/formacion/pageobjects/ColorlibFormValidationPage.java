package com.choucair.formacion.pageobjects;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class ColorlibFormValidationPage extends PageObject {
	// Campo required
	@FindBy(xpath = "//*[@id=\"req\"]")
	public WebElementFacade txtRequired;
	// Campo seleccion deporte 1
	@FindBy(xpath = "//*[@id=\"sport\"]")
	public WebElementFacade cmbSport1;
	// Campo seleccion deporte 2
	@FindBy(xpath = "//*[@id=\"sport2\"]")
	public WebElementFacade cmbSport2;
	// Campo url
	@FindBy(xpath = "//*[@id=\"url1\"]")
	public WebElementFacade txtUrl;
	// Campo email
	@FindBy(xpath = "//*[@id=\"email1\"]")
	public WebElementFacade txtEmail;
	// Campo password
	@FindBy(xpath = "//*[@id=\"pass1\"]")
	public WebElementFacade txtPass;
	// Campo confirmar password
	@FindBy(xpath = "//*[@id=\"pass2\"]")
	public WebElementFacade txtPass2;
	// Campo tamaño minimo
	@FindBy(xpath = "//*[@id=\"minsize1\"]")
	public WebElementFacade txtMinSize;
	// Campo tamaño maximo
	@FindBy(xpath = "//*[@id=\"maxsize1\"]")
	public WebElementFacade txtMaxSize;
	// Campo opcional
	@FindBy(xpath = "//*[@id=\"bootstrap-admin-template\"]")
	public WebElementFacade txtOptional;
	// Campo numero
	@FindBy(xpath = "//*[@id=\"number2\"]")
	public WebElementFacade txtNumber;
	// Campo IP
	@FindBy(xpath = "//*[@id=\"ip\"]")
	public WebElementFacade txtIP;
	// Campo fecha
	@FindBy(xpath = "//*[@id=\"date3\"]")
	public WebElementFacade txtDate;
	// Campo fecha mas temprana
	@FindBy(xpath = "//*[@id=\"past\"]")
	public WebElementFacade txtDateEarlier;
	// Boton validar
	@FindBy(xpath = "//*[@id=\"block-validate\"]/div[10]/input")
	public WebElementFacade btnValidate;
	//Globo Informativo
	@FindBy(xpath = "(//DIV[@class='formErrorContent'])[1]")
	public WebElementFacade globoInformativo;
	
	public void validate() 
	{
		btnValidate.click();
	}
	
	public void form_sin_errores() 
	{
		assertThat(globoInformativo.isCurrentlyVisible(), is(false));
	}
	
	public void form_con_errores() 
	{
		assertThat(globoInformativo.isCurrentlyVisible(), is(true));
	}

	public void Required(String datoPrueba) {
		txtRequired.click();
		txtRequired.clear();
		txtRequired.sendKeys(datoPrueba);
	}

	public void Select_Sport(String datoPrueba) {
		cmbSport1.click();
		cmbSport1.selectByVisibleText(datoPrueba);
	}

	public void Multiple_Select_Sport(String datoPrueba) {
		cmbSport2.selectByVisibleText(datoPrueba);
	}

	public void url(String datoPrueba) {
		txtUrl.click();
		txtUrl.clear();
		txtUrl.sendKeys(datoPrueba);
	}

	public void email(String datoPrueba) {
		txtEmail.click();
		txtEmail.clear();
		txtEmail.sendKeys(datoPrueba);
	}

	public void password(String datoPrueba) {
		txtPass.click();
		txtPass.clear();
		txtPass.sendKeys(datoPrueba);
	}

	public void confirm_password(String datoPrueba) {
		txtPass2.click();
		txtPass2.clear();
		txtPass2.sendKeys(datoPrueba);
	}

	public void minimun_field_size(String datoPrueba) {
		txtMinSize.click();
		txtMinSize.clear();
		txtMinSize.sendKeys(datoPrueba);
	}

	public void maximun_field_size(String datoPrueba) {
		txtMaxSize.click();
		txtMaxSize.clear();
		txtMaxSize.sendKeys(datoPrueba);
	}

	public void number(String datoPrueba) {
		txtNumber.click();
		txtNumber.clear();
		txtNumber.sendKeys(datoPrueba);
	}

	public void ip(String datoPrueba) {
		txtIP.click();
		txtIP.clear();
		txtIP.sendKeys(datoPrueba);
	}

	public void date(String datoPrueba) {
		txtDate.click();
		txtDate.clear();
		txtDate.sendKeys(datoPrueba);
	}

	public void date_Earlier(String datoPrueba) {
		txtDateEarlier.click();
		txtDateEarlier.clear();
		txtDateEarlier.sendKeys(datoPrueba);
	}
}
