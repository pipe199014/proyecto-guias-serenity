package com.choucair.formacion.pageobjects;

import java.io.IOException;
import com.choucair.formacion.utilities.ProcessUtility;
import myextra.*;
import net.serenitybdd.core.pages.PageObject;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class IseriesMyExtraPage extends PageObject {

	public _System sSystem = null;
	public Screen Screen = null;
	public _Session Sess0 = null;
	public int g_HostSettleTime = 3000; // milisegundos

	public void iniciar_Extra(String rutaCalidad) throws InterruptedException {
		try {
			if (ProcessUtility.isProcessRunning("EXTRA.exe"))
				MyExtra_init.cerrarMyExtraAbierto();
			MyExtra_init.abrirArchivoDeMyExtra(rutaCalidad);
			Thread.sleep(5000);
			sSystem = MyExtra_init.crearSistema();
			Sess0 = MyExtra_init.crearSesion(sSystem);
			Screen = MyExtra_init.crearPantalla(Sess0);
			sSystem.timeoutValue(g_HostSettleTime);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void Autenticar_Extra(String strUsuario, String strClave) {
		try {
			String TextoPantalla = "";
			Screen.putString(strUsuario, 6, 53, null);
			Screen.putString(strClave, 7, 53, null);
			Screen.sendKeys("<Enter>");
			Screen.waitHostQuiet(g_HostSettleTime);
			Thread.sleep(2000);
			TextoPantalla = Screen.getString(24, 1, 49, Screen); // capturar texto
			// validación de clave
			assertThat(TextoPantalla, is(not("La información de inicio de sesión no es correcta")));
			int i = 0;
			while (true) {
				i++;
				// verifica label para identificar si se logró la autenticación exitosa
				Thread.sleep(2000);
				TextoPantalla = Screen.getString(6, 7, 9, Screen);
				if (!TextoPantalla.equals("2. Tareas")) {
					Screen.sendKeys("<Enter>");
					Screen.waitHostQuiet(g_HostSettleTime);
				} else {
					break;
				}
				if (i == 5) {
					break;
				}
			}
			assertThat(TextoPantalla, is("2. Tareas"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}
	}

	public void Ejecutar_comandos(String Opcion, String Num_Prestamo) {
		try {

			// Cargo pantalla, cargo ambiente, ejecuto comando
			String strCargarAmbiente = "CALL SUFSDAPU/LOAD_ASE 'WCLSDATA1'";
			String strEjecutar = "CALL SL020 0";
			sSystem = MyExtra_init.crearSistema();
			Sess0 = MyExtra_init.crearSesion(sSystem);
			Screen = MyExtra_init.crearPantalla(Sess0);

			Screen.putString(strCargarAmbiente, 20, 7, null);
			Screen.sendKeys("<Enter>");
			Thread.sleep(5000);

			Screen.putString(strEjecutar, 20, 7, null);
			Screen.sendKeys("<Enter>");
			Thread.sleep(5000);

			// verifica campo para saber si si ingresó a la consulta
			String TextoConsulta = Screen.getString(20, 39, 2, Screen);
			assertThat(TextoConsulta, containsString("01"));

			// Ingreso la opción y el número de prestamo a buscar
			Screen.putString(Opcion, 20, 39, null);
			Screen.putString(Num_Prestamo, 21, 39, null);
			Screen.sendKeys("<Enter>");
			Thread.sleep(5000);

		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}

	}

	public void verificar_consulta_exitosa() throws InterruptedException {
		
		sSystem = MyExtra_init.crearSistema();
		Sess0 = MyExtra_init.crearSesion(sSystem);
		Screen = MyExtra_init.crearPantalla(Sess0);
		
		String ConsultaExitosa = Screen.getString(24, 2, 33, Screen);
		assertThat(ConsultaExitosa, is(not("El credito seleccionado no existe")));
		Thread.sleep(5000);

	}
}
