#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@tag
Feature: Title of your feature
  I want to use this template for my feature file

  @CasoFeliz
    Scenario Outline: Consultar table de clientes CNAME y verificar resultados
    Given Consultar CNAME
    		| <Documento>   | <Npr> | <Npo> | <Age> |
    
    Examples:
    | Documento | Npr              | Npo   | Age |
    | 900083    | 00000000000000000| 316   | 300 | 
    
    @MYEXTRA
    Scenario: Trabajar con el objeto MyExtra
    Given Abrir MyExtra "C:\\Users\\fgomezv\\Desktop\\SUFIQUA.EDP"
    When Autenticar MyExtra
    When Ejecuto los comandos para la consulta del prestamo
    | Opcion | Num_Prestamo      |
    | 01     | 00000000000000035 |
    Then Verifico consulta exitosa
    
    